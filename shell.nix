{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
  buildInputs = [
    pkgs.gnuplot
    (pkgs.python3.withPackages (p: [p.requests p.pytz]))
  ];
}
