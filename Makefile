NIX_PATH=nixpkgs=https://releases.nixos.org/nixos/19.09/nixos-19.09.840.8bf142e001b/nixexprs.tar.xz
.PHONY: build

build:
	NIX_PATH=$(NIX_PATH) nix-shell --pure --run "python distances.py"
